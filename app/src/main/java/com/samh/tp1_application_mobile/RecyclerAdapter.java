package com.samh.tp1_application_mobile;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.samh.tp1_application_mobile.R;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private com.samh.tp1_application_mobile.data.Country[] countries = com.samh.tp1_application_mobile.data.Country.countries;

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView nomPays;
        TextView capitalPays;

        ViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            nomPays = itemView.findViewById(R.id.nomPays);
            capitalPays = itemView.findViewById(R.id.capitalPays);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i){
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i){
        viewHolder.capitalPays.setText(countries[i].getCapital());
        viewHolder.nomPays.setText(com.samh.tp1_application_mobile.data.Country.countries[i].getName());
        String uri =  countries[i].getImgUri();
        Context c = viewHolder.itemImage.getContext();
        viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri, null , c.getPackageName())));
    }

    @Override
    public int getItemCount() {
        return countries.length;
    }


}
