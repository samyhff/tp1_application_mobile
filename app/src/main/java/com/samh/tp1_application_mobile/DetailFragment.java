package com.samh.tp1_application_mobile;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.samh.tp1_application_mobile.data.Country;

public class DetailFragment extends Fragment {

    DetailFragmentArgs paysId;
    ImageView itemImage;
    TextView nomPays;
    TextView capitalPays;
    TextView languePays;
    TextView monnaiePays;
    TextView populationPays;
    TextView superficiePays;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        paysId = paysId.fromBundle(getArguments());
        Country pays = Country.countries[paysId.getCountryId()];
        itemImage = view.findViewById(R.id.paysImage);
        nomPays = view.findViewById(R.id.nomText);
        capitalPays = view.findViewById(R.id.capitalText);
        languePays = view.findViewById(R.id.langueText);
        monnaiePays = view.findViewById(R.id.monnaieText);
        populationPays = view.findViewById(R.id.populationText);
        superficiePays = view.findViewById(R.id.superficieText);


        String uri =  pays.getImgUri();
        Context c = itemImage.getContext();
        itemImage.setImageDrawable(c.getResources().getDrawable(
                c.getResources(). getIdentifier (uri, null , c.getPackageName())));
      nomPays.setText(pays.getName());
      capitalPays.setText(pays.getCapital());
      languePays.setText(pays.getLanguage());
      monnaiePays.setText(pays.getCurrency());
      Integer t = pays.getPopulation();
      populationPays.setText(t.toString());
      t = pays.getArea();
      superficiePays.setText(t.toString() + " km2");

        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });
    }
}